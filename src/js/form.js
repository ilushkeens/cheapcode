(function ($) {
  'use strict'

  // контроль отправки форм
  $('form').each(function () {
    let form = $(this),
        button = form.find('.button_send')

    // функция проверки заполнения обязательных полей формы
    function checkInput() {
      form.find('textarea, input').each(function () {
        let field = $(this)

        // проверяет обязательно ли поле к заполнению
        if (field.attr('required') !== undefined) {
          if (field.val() != '') {
            // eсли поле не пустое удаляет класс-указание
            field.removeClass('empty')
          } else {
            // eсли поле пустое добавляет класс-указание
            field.addClass('empty')
          }
        }
      })
    }

    // функция подсветки незаполненных полей
    function lightEmpty() {
      // добавляет подсветку
      form.find('.empty').addClass('error')

      // ..и через 1.5 секунды её удаляет
      setTimeout(function () {
        form.find('.empty').removeClass('error')
      }, 1500)
    }

    // проверяет форму в режиме реального времени
    setInterval(function () {
      checkInput()

      // считает количество незаполненных полей
      let countEmpty = form.find('.empty').length

      // добавляет условие-тригер к кнопке отправки формы
      if (countEmpty > 0) {
        if(button.hasClass('button_disable')) {
          return false
        } else {
          button.addClass('button_disable')
        }
      } else {
        button.removeClass('button_disable')
      }
    }, 1000)

    function sendForm(type, description, email, phone) {
      $.ajax({
        url: 'https://cheapcode.ru/api/cheapcode/request',
        method: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({
          'type_': type,
          'description': description,
          'email': email,
          'phone': phone
        })
      }).done(function () {
        form.find('.button a').text('Запрос отправлен').attr('title', 'Запрос отправлен').addClass('send')

        setTimeout(function () {
          form.find('.button a').text('Отправить запрос').attr('title', 'Отправить запрос').removeClass('send')
        }, 5000)
      })
    }

    // обрабатывает попытку отправки формы
    button.on('click', 'a', function (event) {
      event.preventDefault()

      let type = form.find('input[name="type"]:checked').val() ?? form.find('input[name="type"]').val(),
          email = form.find('input[name="email"]').val(),
          phone = form.find('input[name="phone"]').val(),
          description = form.find('textarea[name="description"]').val()

      if (button.hasClass('button_disable')) {
        // подсвечивает незаполненные поля
        lightEmpty()
        return false
      } else {
        // отправляет форму
        sendForm(type, description, email, phone);
        form.find('input').val('')
        form.find('textarea').val('')
        button.addClass('button_disable')
      }
    })
  })
}(jQuery))
